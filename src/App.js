import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
// import { connect } from "react-redux";

import Home from "./views/Home/Home";
import Signup from "./views/Signup/Signup";
import Login from "./views/Login/Login";
import UserHome from "./views/User/UserHome/UserHome";
import CreateConversation from "./views/User/CreateConversation/CreateConversation";
import ViewConversation from "./views/User/ViewConversation/ViewConversation";
import NotFound from "./views/NotFound/NotFound";

import "./App.scss";

class App extends Component {
	render() {
		return (
			<div className="App">
				<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/signup" component={Signup} />
					<Route exact path="/login" component={Login} />
					<Route path="/home" component={UserHome} />
					<Route path="/create" component={CreateConversation} />
					<Route path="/view" component={ViewConversation} />
					<Route component={NotFound} />
				</Switch>
			</div>
		);
	}
}

export default App;
// export default connect(
// 	state => state,
// 	{}
// )(App);
