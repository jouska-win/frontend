import { combineReducers } from "redux";

import reducer from "./index";
import errorsReducer from "./errors";
import conversationsReducer from "./conversations";

const rootReducer = combineReducers({
	main: reducer,
	errors: errorsReducer,
	conversations: conversationsReducer
});

export default rootReducer;
