const reducer = (state = {}, action) => {
	switch (action.type) {
		// case "USER_CREATE":
		// 	console.log("USER_CREATE in main reducer", action);
		// 	return { ...state, email: action.email, password: action.password };
		// case "USER_CREATE_SUCCESS":
		// 	console.log("USER_CREATE_SUCCESS in main reducer", action);
		// 	return { ...state, email: action.email, password: action.password };
		// case "USER_LOGIN":
		// 	console.log("USER_LOGIN in main reducer", action);
		// 	return { ...state, authenticated: true };
		case "USER_LOGIN_SUCCESS":
			console.log("USER_LOGIN_SUCCESS in main reducer", action);
			return { ...state, authenticated: true };
		case "USER_LOGOUT":
			console.log("USER_LOGOUT in main reducer", action);
			return { ...state, authenticated: false };
		case "USER_LOGOUT_SUCCESS":
			console.log("USER_LOGOUT_SUCCESS in main reducer", action);
			return { ...state, authenticated: false };

		default:
			// console.log("default in main reducer", action);
			return state;
	}
};
export default reducer;
