const conversationsReducer = (state = [], action) => {
	switch (action.type) {
		case "FETCH_CONVERSATIONS":
			// console.log("conversations reducer in FETCH_CONVERSATIONS");
			return state;

		case "FETCH_CONVERSATIONS_SUCCESS":
			// console.log(
			// 	"conversations reducer in FETCH_CONVERSATIONS_SUCCESS",
			// 	action
			// );
			return action.conversations;

		case "CREATE_CONVERSATION":
			// console.log("conversations reducer in CREATE_CONVERSATION");
			return state;

		case "VIEW_CONVERSATION":
			// console.log("conversations reducer in VIEW_CONVERSATION");
			return state;

		case "VIEW_CONVERSATION_SUCCESS":
			// console.log("conversations reducer in VIEW_CONVERSATION_SUCCESS", action);
			return [action.conversation];

		case "EDIT_CONVERSATION":
			// console.log("conversations reducer in EDIT_CONVERSATION");
			return state;

		case "DELETE_CONVERSATION":
			// console.log("conversations reducer in DELETE_CONVERSATION");
			return state;

		default:
			// console.log("conversations reducer in default");
			return state;
	}
};

export default conversationsReducer;
