import { ERROR_ADD, ERROR_REMOVE, ERROR_RESET } from "../actions/index";

export const errorAddCreator = errorMsg => {
	console.log("errorAddCreator", errorMsg);
	return {
		type: ERROR_ADD,
		errorMsg
	};
};

export const errorRemoveCreator = errorMsg => {
	console.log("errorRemoveCreator", errorMsg);
	return {
		type: ERROR_REMOVE,
		errorMsg
	};
};

export const errorResetCreator = () => {
	console.log("errorResetCreator");
	return {
		type: ERROR_RESET
	};
};
