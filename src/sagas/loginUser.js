import { call, put, takeEvery } from "redux-saga/effects";
import axios from "axios";

function* userLoginWorker(action) {
	try {
		yield call(console.log, "userLogin SAGA", action);
		const { data, status } = yield call(axios.post, "/api/web/login", {
			...action
		});
		if (status === 200 && data === "Login successful") {
			yield put({
				type: "USER_LOGIN_SUCCESS",
				action
			});
		}
		// TODO: this can be a flash message instead
	} catch (e) {
		if (e.message === "Request failed with status code 401")
			yield put({
				type: "ERROR_ADD",
				errorMsg: "Incorrect password"
			});
		else if (e.message === "Request failed with status code 404")
			yield put({
				type: "ERROR_ADD",
				errorMsg: "There is no registered user with that email"
			});
		yield call(console.log, "Error at userLoginWorker", e.message);
	}
}

function* userLoginWatcher() {
	yield takeEvery("USER_LOGIN", userLoginWorker);
}

export default userLoginWatcher;
