import { call, takeEvery } from "redux-saga/effects";
import axios from "axios";

function* createUserWorker(action) {
	try {
		yield call(console.log, "createUser SAGA", action);
		yield call(axios.post, "/api/signup", {
			...action
		});
	} catch (e) {}
}

function* createUserWatcher() {
	yield takeEvery("USER_CREATE", createUserWorker);
}

export default createUserWatcher;
