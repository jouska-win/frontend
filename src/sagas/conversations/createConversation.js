import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";

function* createConversationWorker(action) {
	try {
		yield call(console.log, "createConversationWorker action is", action);
		const data = yield call(axios.put, "/api/web/create", {
			...action
		});
		yield call(console.log, "data is", data);
	} catch (err) {
		yield call(console.log, "error in createConversationWorker", err);
		yield put({
			type: "ERROR_ADD",
			errorMsg: "Couldn't create your conversation :("
		});
	}
}

function* createConversationWatcher() {
	yield takeLatest("CREATE_CONVERSATION", createConversationWorker);
}

export default createConversationWatcher;
