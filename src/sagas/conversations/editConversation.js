import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";

function* editConversationWorker(action) {
	try {
		yield call(console.log, "EDIT_CONVERSATION try", action);
		const data = yield call(axios.post, "/api/web/edit", {
			...action
		});
		yield call(console.log, "data is", data);
	} catch (e) {
		yield put({
			type: "ERROR_ADD",
			errorMsg: "Couldn't save the edit to your conversation :/"
		});
	}
}

function* editConversationWatcher() {
	yield takeLatest("EDIT_CONVERSATION", editConversationWorker);
}

export default editConversationWatcher;
