import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Cookies from "universal-cookie";

const cookies = new Cookies();

const ProtectedRoute = props => {
	// if there is no access-token cookie, redirect to login to get oen
	// if there is an access token cookie render children, where we will send a request to verify the token
	return cookies.get("access-token") ? (
		props.children
	) : (
		<Redirect to="/login" />
	);
};

export default connect(
	({ main: authenticated }) => authenticated,
	{}
)(ProtectedRoute);
