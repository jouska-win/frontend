import React from "react";
import { connect } from "react-redux";

import Input from "../../Input/Input";
import Button from "../../Button/Button";
import styles from "../ModalForm.module.scss";
import {
	errorAddCreator,
	errorRemoveCreator,
	errorResetCreator
} from "../../../actionCreators/errors";
import { userLoginCreator } from "../../../actionCreators/index";

const LoginForm = props => {
	return (
		<div className={styles["form-container"]}>
			<form
				className={styles.form}
				onSubmit={e => {
					e.preventDefault();
					let {
						elements,
						elements: { email }
					} = e.target;
					// debugger;
					if (email.value === "" || props.emailRegex.test(email.value)) {
						props.resetErrors();
						props.addError("Please enter a valid email address");
					} else {
						props.resetErrors();
						let action = {};
						for (let el of elements) {
							action[el.name] = el.value;
						}
						props.loginUser(action);
						// redirect to /home
					}
				}}
			>
				{props.errors.map((err, i) => (
					<span key={i} className={styles["error-message"]}>
						{err}
					</span>
				))}
				<h3>Log in with email</h3>
				<Input
					label="email"
					labelText="Email"
					type="email"
					placeholder="Enter your email address"
				/>
				<Input
					label="password"
					labelText="Password"
					type="password"
					placeholder="Enter your password"
				/>
				<Button type="submit" text="Login" />
			</form>
		</div>
	);
};

export default connect(
	({ main, errors }) => ({ ...main, errors }),
	dispatch => ({
		addError: errorMsg => dispatch(errorAddCreator(errorMsg)),
		removeError: errorMsg => dispatch(errorRemoveCreator(errorMsg)),
		resetErrors: () => dispatch(errorResetCreator()),
		loginUser: action => dispatch(userLoginCreator(action))
	})
)(LoginForm);
