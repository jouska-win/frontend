import React from "react";

import styles from "./Tag.module.scss";

const Tag = props => {
	return <div className={styles.tag}>{props.text}</div>;
};

export default Tag;
