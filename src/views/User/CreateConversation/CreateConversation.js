import React, { Component } from "react";
import Editor from "react-medium-editor";
import { connect } from "react-redux";

import "../../../assets/styles/editor.scss";
import "../../../assets/styles/editorStyle.scss";

import ProtectedRoute from "../../../components/ProtectedRoute/ProtectedRoute";
import UserNavbar from "../../components/UserNavbar/UserNavbar";
import Button from "../../../components/Button/Button";
import Footer from "../../../components/Footer/Footer";
import { createConversationCreator } from "../../../actionCreators/conversations";

import styles from "./CreateConversation.module.scss";

class CreateConversation extends Component {
	state = {
		title: "",
		description: "",
		text: "",
		tags: [""],
		base64Image: "",
		imageName: ""
	};
	updateValue = val => e => {
		e.persist();
		this.setState((prevState, prevProps) => ({
			[val]: e.target.value
		}));
	};
	handleChange = text => {
		this.setState((prevState, prevProps) => ({
			text
		}));
	};
	render() {
		let imageRef = React.createRef();
		const fileReader = new FileReader();
		const handleImage = e => {
			fileReader.readAsDataURL(imageRef.current.files[0]);
			fileReader.onload = e => {
				this.setState((prevState, prevProps) => ({
					base64Image: e.target.result,
					imageName: imageRef.current.files[0].name
				}));
			};
		};
		return (
			<ProtectedRoute>
				<div className={styles["create"]}>
					<div className={styles.navbar}>
						<UserNavbar type="light" />
					</div>
					<div className={styles.main}>
						<div className={styles["input-group"]}>
							<input
								className={styles["input-title"]}
								name="title"
								type="text"
								required
								placeholder="Conversation title"
								onChange={this.updateValue("title")}
							/>
							<input
								className={styles["input-description"]}
								type="text"
								required
								placeholder="Description"
								size="100"
								onChange={this.updateValue("description")}
							/>
						</div>
						<div className={styles["image-container"]}>
							<span>Choose conversation image</span>
							<input type="file" ref={imageRef} onChange={handleImage} />
						</div>
						<div className={styles.conversation}>
							<Editor
								data-placeholder={
									"How did your conversation begin?\nType in your answer here..."
								}
								text={this.state.text}
								onChange={this.handleChange}
							/>
							<div className={styles["conversation-options"]}>
								<Button to="/home" type="outline" text="Cancel" />
								<Button
									to="/home"
									type="primary"
									text="Save"
									onClick={() =>
										this.props.createConversation({
											...this.state
										})
									}
								/>
							</div>
						</div>
					</div>
					<div className={styles.footer}>
						<Footer />
					</div>
				</div>
			</ProtectedRoute>
		);
	}
}

export default connect(
	state => state,
	dispatch => ({
		createConversation: action => dispatch(createConversationCreator(action))
	})
)(CreateConversation);
