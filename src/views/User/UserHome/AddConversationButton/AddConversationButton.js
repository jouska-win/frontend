import React from "react";
import { Link } from "react-router-dom";

import addBtn from "../../../../assets/images/btn-add-secondary.svg";
import styles from "./AddConversationButton.module.scss";

const AddConversationButton = props => {
	return (
		<Link to="/create">
			<img
				className={styles["btn-add-conversation"]}
				src={addBtn}
				alt="Create a new conversation add button"
			/>
		</Link>
	);
};

export default AddConversationButton;
