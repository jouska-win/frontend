import React, { Component } from "react";
import { connect } from "react-redux";

import ProtectedRoute from "../../../components/ProtectedRoute/ProtectedRoute";
import UserNavbar from "../../components/UserNavbar/UserNavbar";
import Conversation from "./Conversation/Conversation";
import AddConversationButton from "./AddConversationButton/AddConversationButton";
import Footer from "../../../components/Footer/Footer";
import { fetchConversationsCreator } from "../../../actionCreators/conversations";

import styles from "./UserHome.module.scss";

class UserHome extends Component {
	componentDidMount() {
		this.props.fetchConversations();
	}

	render() {
		const { conversations } = this.props;
		return (
			<ProtectedRoute>
				<div className={styles["user-home"]}>
					<div className={styles.navbar}>
						<UserNavbar />
					</div>
					<div className={styles.main}>
						<div className={styles["conversations-container"]}>
							{conversations.reverse().map(c => {
								return (
									<Conversation
										key={c.id}
										id={c.id}
										title={c.title}
										description={c.description}
										text={c.text}
										tags={c.tags}
										image={c.image}
										createdAt={c.createdAt}
										size={Math.random() <= 0.2 ? "lg" : ""}
									/>
								);
							})}
						</div>
						<AddConversationButton />
					</div>
					<div className={styles.footer}>
						<Footer />
					</div>
				</div>
			</ProtectedRoute>
		);
	}
}

export default connect(
	state => state,
	dispatch => ({
		fetchConversations: () => dispatch(fetchConversationsCreator())
	})
)(UserHome);
