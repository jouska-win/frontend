import React, { Component } from "react";
import Modal from "react-modal";
import { connect } from "react-redux";

import styles from "./Signup.module.scss";
import Navbar from "../../components/Navbar/Navbar";
import Button from "../../components/Button/Button";
import Footer from "../../components/Footer/Footer";
import signupImage from "../../assets/images/undraw_queue_secondary.svg";
import { userCreateCreator } from "../../actionCreators/index";
import ModalForm from "../../components/ModalForm/ModalForm";

const customStyles = {
	content: {
		top: "50%",
		left: "50%",
		right: "auto",
		bottom: "auto",
		marginRight: "-50%",
		transform: "translate(-50%, -50%)",
		width: "642px",
		height: "600px",
		padding: "50px 71px",
		boxSizing: "border-box",
		boxShadow:
			"0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14),		0 1px 5px 0 rgba(0, 0, 0, 0.12)"
	}
};

Modal.setAppElement("#root");

class Signup extends Component {
	state = {
		modalIsOpen: false
	};

	openModal = () => {
		this.setState({ modalIsOpen: true });
	};

	afterOpenModal = () => {
		// references are now sync'd and can be accessed.
		// this.subtitle.style.color = "#f00";
	};

	closeModal = () => {
		this.setState({ modalIsOpen: false });
	};
	render() {
		return (
			<div className={styles.signup}>
				<div className={styles.navbar}>
					<Navbar />
				</div>
				<div className={styles.main}>
					<div className={styles.container}>
						<img src={signupImage} alt="Sign up illustration" />
						<div className={styles["signup-options"]}>
							<Button
								to="/signup/google"
								type="google"
								text="Sign up with Google"
								size="lg"
							/>
							<Button
								to="/signup/facebook"
								type="facebook"
								text="Sign up with Facebook"
								size="lg"
							/>
							<Button
								to="/signup/spotify"
								type="spotify"
								text="Sign up with Spotify"
								size="lg"
							/>
							<Button
								// to="/signup/email"
								type="outline"
								text="Sign up with Email"
								size="lg"
								onClick={this.openModal}
							/>
						</div>
						<Modal
							isOpen={this.state.modalIsOpen}
							onAfterOpen={this.afterOpenModal}
							onRequestClose={this.closeModal}
							style={customStyles}
							contentLabel="Example Modal"
						>
							<ModalForm type="signup" createUser={this.props.createUser} />
						</Modal>
					</div>
				</div>
				<div className={styles.footer}>
					<Footer />
				</div>
			</div>
		);
	}
}

export default connect(
	state => ({ state }),
	dispatch => ({
		createUser: action => dispatch(userCreateCreator(action))
	})
)(Signup);
