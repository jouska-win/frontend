import React from "react";

import styles from "./NotFound.module.scss";
import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";
import notFoundImage from "../../assets/images/undraw_Camping-secondary.svg";

const NotFound = props => (
	<div className={styles["not-found"]}>
		<div className={styles.navbar}>
			<Navbar />
		</div>
		<div className={styles.main}>
			<div className={styles.container}>
				<p className={styles["error-text"]}>
					I went out looking for a page, but I found myself on a scooter
					<br />
					in a forest
					<br />
					near a camp
					<br />
					trying to escape a hillbilly
					<br />
					<br />
					Now how did that happen...
				</p>
				<img src={notFoundImage} alt="Sign up illustration" />
			</div>
		</div>
		<div className={styles.footer}>
			<Footer />
		</div>
	</div>
);

export default NotFound;
